import os
import shutil
import pandas as pd
from jobs import task1


class TestTask1:

    ##################### Test Transform data #####################
    def test_transform_data(self, spark_session):

        test_data = spark_session.createDataFrame(
             [(
                {"_value": "MPs should attend all debates, not merely turn up and vote or strike pairing deals. With other commitments, a five day Commons is not workable for MPs: I suggest three full days (9am to 6pm minimum), with one or two days for Committees, leaving at least one day for constituency work."},
                {"_value": "Reform the Commons: Three days full time with compulsory attendance for all MPs."},
                27
            )],
            ["abstract", "label", "numberOfSignatures"],
        )

        expected_data = spark_session.createDataFrame(
            [
                (0, 51, 13, 27)
            ],
            ["petition_id", "abstract_length", "label_length", "num_signatures"]
        ).toPandas()

        real_data = task1._transform_data(test_data).toPandas()

        pd.testing.assert_frame_equal(
            real_data.drop("petition_id", axis=1), expected_data.drop("petition_id", axis=1), check_dtype=False)

    
    ##################### Test Run job #####################
    def test_run_job(self, spark_session, mocker):
        test_config = {"app_name": "PetitionETL",
                       "source_data_path": "data/input_data.json",
                       "output_data_path": "./tests/",
                       "columns_of_interest": ["abstract"],
                       "other_columns": ["petition_id"]
                       }

        test_data = spark_session.createDataFrame(
            [(
                {"_value": "MPs should attend all debates, not merely turn up and vote or strike pairing deals. With other commitments, a five day Commons is not workable for MPs: I suggest three full days (9am to 6pm minimum), with one or two days for Committees, leaving at least one day for constituency work."},
                {"_value": "Reform the Commons: Three days full time with compulsory attendance for all MPs."},
                27
            )],
            ["abstract", "label", "numberOfSignatures"],
        )

        mocker.patch.object(task1, "_extract_data")
        task1._extract_data.return_value = test_data
        task1.run_job(spark_session, test_config)
        assert os.path.exists(test_config.get(
            "output_data_path") + "output1")
