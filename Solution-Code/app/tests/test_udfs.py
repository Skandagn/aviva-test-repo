from shared import udfs
from pyspark.sql import Row

class TestUDFs:
    def test_cWordCount(self):
        common_word_list = ['should', 'their', 'people', 'would', 'which', 'being', 'government', 'there', 'children',
                            'other', 'public', 'these', 'Government', 'years', 'British', 'There', 'those', 'could', 'needs', 'about']
        column_of_interest = 'abstract'
        other_columns = ['petition_id']
        
        partitionData = [Row(abstract="MPs should attend all debates, not merely turn up and vote or strike pairing deals. With other commitments, a five day Commons is not workable for MPs: I suggest three full days (9am to 6pm minimum), with one or two days for Committees, leaving at least one day for constituency work.", petition_id=0)]

        expected_value = [{'petition_id': 0, 'should': 1, 'their': 0, 'people': 0, 'would': 0, 'which': 0, 'being': 0, 'government': 0, 'there': 0, 'children': 0, 'other': 1, 'public': 0, 'these': 0, 'Government': 0, 'years': 0, 'British': 0, 'There': 0, 'those': 0, 'could': 0, 'needs': 0, 'about': 0}]

        assert list(udfs.cWordCount(
            common_word_list, column_of_interest, other_columns)(partitionData)) == expected_value
