from pyspark.sql.functions import col, split, explode
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql.functions import col
from shared.udfs import cWordCount, removePunctuations

schema = StructType([
    StructField("abstract", StructType(
        [StructField("_value", StringType(), True)]), True),
    StructField("label", StructType(
        [StructField("_value", StringType(), True)]), True),
    StructField("numberOfSignatures", IntegerType(), True),
])


def _extract_data(spark, config):
    """ Load data from given json input file """

    return spark.read.schema(schema).json(f"{config.get('source_data_path')}")


def _transform_data(raw_df, config):
    """ Transform raw dataframe """

    column_of_interest = ""
    columns_of_interest = config.get("columns_of_interest")
    other_columns = config.get("other_columns")

    # Adding petition-id and removing punctuations
    raw_df = raw_df.withColumn(
        "petition_id", f.monotonically_increasing_id().alias("petition_id"))
    raw_df = raw_df.select(col('abstract._value').alias("abstract"), col('label._value').alias(
        "label"), col('numberOfSignatures').alias('num_signatures'), "petition_id")

    # Removing punctuations
    for col_name in ["abstract", "label"]:
        raw_df = raw_df.withColumn(
            col_name, removePunctuations(col_name))

    # Checking for more than one column of interest, if yes, then join column values into one string
    if len(columns_of_interest) > 1:
        raw_df = raw_df.select(f.concat_ws(
            ' ', *columns_of_interest).alias("concatenated_column"), *other_columns)
        column_of_interest = "concatenated_column"
    else:
        column_of_interest = columns_of_interest[0]
        raw_df = raw_df.select(column_of_interest, *other_columns)

    # Counting top 20 words in each column
    top_20_words = raw_df.withColumn('word', f.explode(f.split(f.col(column_of_interest), ' '))) \
        .filter(f.length(col('word')) >= 5) \
        .groupBy('word') \
        .count() \
        .sort('count', ascending=False) \
        .limit(20)

    common_word_list = top_20_words.select("word").rdd \
        .flatMap(lambda x: x) \
        .collect()

    # Creating a new column with word count for those 20 top words
    return raw_df.rdd.mapPartitions(cWordCount(common_word_list, column_of_interest, other_columns)) \
        .toDF() 



def _load_data(config, transformed_df):
    """ Save data to csv file """

    transformed_df = transformed_df.coalesce(1)
    transformed_df.write.mode("overwrite").option('header', 'true').csv(
        f"{config.get('output_data_path')}/output2"
    )


def run_job(spark, config):
    """ Run Task 2 job """

    _load_data(config, _transform_data(_extract_data(spark, config), config))
