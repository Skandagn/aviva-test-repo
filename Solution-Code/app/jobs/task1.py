from pyspark.sql.functions import col, split, explode
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql.functions import col
from shared.udfs import removePunctuations, countWords

schema = StructType([
    StructField("abstract", StructType(
        [StructField("_value", StringType(), True)]), True),
    StructField("label", StructType(
        [StructField("_value", StringType(), True)]), True),
    StructField("numberOfSignatures", IntegerType(), True),
])


def _extract_data(spark, config):
    """ Load data from given json input file """

    return spark.read.schema(schema).json(f"{config.get('source_data_path')}")


def _transform_data(raw_df):
    """ Transform raw dataframe """
    
    # Adding petition-id
    raw_df = raw_df.withColumn(
        "petition_id", f.monotonically_increasing_id().alias("petition_id"))
    raw_df = raw_df.select(col('abstract._value').alias("abstract"), col('label._value').alias(
        "label"), col('numberOfSignatures').alias('num_signatures'), "petition_id")
    

    # Re-partition for better performance
    raw_df = raw_df.repartition(20)

    # Removing punctuations
    for col_name in ["abstract", "label"]:
        raw_df = raw_df.withColumn(
            col_name, removePunctuations(col_name))

    # Counting words in each column
    raw_df = raw_df.withColumn(
        'label_length', countWords("label"))
    raw_df = raw_df.withColumn(
        'abstract_length', countWords("abstract"))

    return raw_df.select(
        "petition_id", "abstract_length", "label_length", "num_signatures")


def _load_data(config, transformed_df):
    """ Save data to csv file """

    transformed_df = transformed_df.coalesce(1)
    transformed_df.write.mode("overwrite").option('header', 'true').csv(
        f"{config.get('output_data_path')}/output1"
    )


def run_job(spark, config):
    """ Run Task 1 job """

    _load_data(config, _transform_data(_extract_data(spark, config)))
