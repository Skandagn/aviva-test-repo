from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from pyspark.sql.functions import regexp_replace, trim, col, lower, split, size

##################### udf (Common word count column) #####################
def _cWordCount(common_word_list, column_of_interest, other_columns):
    """ 
    Counts the number of times a word (present in common_word_list) appears in specified column.
    Input: 
        common_word_list: list of words to be counted (extracted top 20 words)
        column_of_interest: column name we need to count words in (Ex: abstract, concatenated_column - in case of multiple columns chosen)
        other_columns: List of other columns to be displayed along with 20 columns (Ex: petition_id, label, num_signatures)    
    Output:
        Each partition returns a list of rows with 20 columns and `other_columns` mentioned
    """
    def udf_cWordCount(partitionData):
        for row in partitionData:
            cWords = {word: 0 for word in common_word_list}
            for word in row[column_of_interest].split(" "): 
                if word in cWords.keys(): 
                    cWords[word] = cWords[word] + 1
            yield {**dict({other_column: row[other_column] for other_column in other_columns}), **cWords}
    return udf_cWordCount

cWordCount = _cWordCount


##################### Remove punctuations #####################
removePunctuations = lambda x: lower(trim(regexp_replace(x,'\\p{Punct}','')))

##################### Count words #####################
countWords = lambda x: size(split(x, ' '))
