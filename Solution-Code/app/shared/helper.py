# Helper functions
def sparkShape(dataFrame):
    """ Prints the shape of a dataframe """
    return (dataFrame.count(), len(dataFrame.columns))